#!/bin/bash

set -ev

# Use the Qt online installer - it savely installs several Qt version
# But it uses a lot more disk space

export QT_QPA_PLATFORM=minimal
QT=qt-unified-linux-x64-online.run
curl -sL --retry 10 --retry-delay 10 -o /tmp/$QT https://download.qt.io/official_releases/online_installers/$QT
chmod +x /tmp/$QT
/tmp/$QT -v --script /tmp/qt_installer.qs LINUX=true
rm -f /tmp/$QT



# The following version uses a lot less disk space (~1.4 GB)
#
# But as the rpath and qmake version path is not corrected and
# it only does a workaround setting one link from /home/wrok/qt/install.
# That only workaround only works for one installed Qt version

# Download pre build Qt libraries from the Qt server, install them
# and set them to use the open source license

# QT_SERVER=http://download.qt.io
# BASE_DIR=online/qtsdkrepository/linux_x64/android/
# QT_VERSION_DIR=qt5_5124
# DIR_START=qt.qt5.5124.
# DIR_END=.android_armv7
# FILE_START=5.12.4-0-201906140209
# FILE_END=-Linux-RHEL_7_4-Clang-Android-Android_ANY-ARMv7.7z
# FILE_END_C=
# MAIN_DIR=qt.qt5.5124.android_armv7
# TARGET_DIR=/opt/Qt/5.12.4/android_armv7
# 
# # These are in separate directories
# COMPONENTS1=""
# 
# # These are in the main directory
# COMPONENTS2="qtxmlpatterns
# qtwebview
# qtwebsockets
# qtwebchannel
# qttranslations
# qttools
# qtsvg
# qtspeech
# qtserialport
# qtsensors
# qtscxml
# qtremoteobjects
# qtquickcontrols2
# qtquickcontrols
# qtmultimedia
# qtlocation
# qtimageformats
# qtgraphicaleffects
# qtgamepad
# qtdeclarative
# qtconnectivity
# qtcanvas3d
# qtbase
# qtandroidextras
# qt3d"
# 
# # Tools
# COMPONENTS3=""
# 
# installQt()
# {
#     mkdir -p /opt/Qt
#     cd /opt/Qt
#     
#     QT_URL=$QT_SERVER/$BASE_DIR/$QT_VERSION_DIR
# 
#     for COMPONENT in $COMPONENTS1
#     do
#         FILE=$FILE_START$COMPONENT$FILE_END
#         wget -q $QT_URL/$DIR_START$COMPONENT$DIR_END/$FILE
#         p7zip -d $FILE
#     done
# 
#     for COMPONENT in $COMPONENTS2
#     do
#         FILE=$FILE_START$COMPONENT$FILE_END
#         wget -q $QT_URL/$MAIN_DIR/$FILE
#         p7zip -d $FILE
#     done
# 
#     # Tools
#     FILE=$FILE_START
#     wget -q $QT_URL/$MAIN_DIR/
#     for COMPONENT in $COMPONENTS3
#     do
#         FILE=$FILE_START$COMPONENT$FILE_END_C
#         wget -q $QT_URL/$MAIN_DIR/$FILE
#         p7zip -d $FILE
#     done
# 
# 
#     # RPath etc. is set to /home/qt/work/install/lib - so doing a symlink
#     mkdir -p /home/qt/work
#     ln -s $TARGET_DIR /home/qt/work/install
# 
#     # switch Qt license to OSS
#     #/opt/Qt/5.12.0/gcc_64/mkspecs/qconfig.pri
#     #QT_EDITION = Enterprise -> OpenSource
#     #QT_LICHECK = licheck64 -> ""
#     sed -i 's/Enterprise/OpenSource/g' $TARGET_DIR/mkspecs/qconfig.pri
#     sed -i 's/QT_LICHECK = licheck64/QT_LICHECK =/g' $TARGET_DIR/mkspecs/qconfig.pri
# 
#     echo "Qt is now configured to be used as OSS"
#     cat $TARGET_DIR/mkspecs/qconfig.pri
# }
# 
# #
# # install Qt 5.12.4 for android 32 bit
# installQt
# 
# #
# # install Qt 5.12.4 for android 64 bit
# DIR_END=.android_arm64_v8a
# FILE_END=-Linux-RHEL_7_4-Clang-Android-Android_ANY-ARM64.7z
# MAIN_DIR=qt.qt5.5124.android_arm64_v8a
# TARGET_DIR=/opt/Qt/5.12.4/android_arm64_v8a
# 
# installQt
